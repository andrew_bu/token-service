# Тестовое задание

Реализовать HTTP сервис для работы с токенами. Рассказать об известных компромиссах и проблемах полученного решения. Язык программирования: PHP, Go или Java.

## Что должен уметь

1. Выпускать токен, если получены верные логин и пароль. Выпуск токена подразумевает сохранение его в БД и возврат пользователю в теле ответа. Проверка реквизитов происходит сравнением присланных данных с реквизитами, хранящимися в БД. Для токенов и реквизитов можно использовать любую БД любого типа. Токен должен представлять из себя хэш от логина, времени выпуска и соли.

2. Возвращать логин владельца токена по токену.

## Пример спецификации

```gherkin
Scenario: Issue a token with correct credentials
    Given there is a user John with password 12345
    When I issue a token as John with password 12345
    Then I see response with valid token with code 202

Scenario: Issue a token with wrong credentials
    Given there is a user John with password 12345
    When I issue a token as John with password 12346
    Then I see response "Wrong user credentials" with code 400
    
Scenario: Get user login by token
    Given there is a user John with token 6b9b9319
    When I ask for user login with token 6b9b9319
    Then I see response "John" with code 200

Scenario: Get user login by unexisting token
    Given there is a user John with token 6b9b9319
    When I ask for user login with token 7b9b9319
    Then I see response "Not Found" with code 404
```

# Запуск приложения

Из консоли:
```
./mvnw spring-boot:run
```
Либо можно из IDEA, вызовом метода `ru.ortex.TokenServiceApplication.main`

Перейти по адресу [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

Для демонстрации добавлены 2 пользователя:

John 12345

George 123456

## Компромиссы и проблемы полученного решения

Среди проблем можно отметить отсутствие возможности инвалидации токена в случаи его компроментации.

Так же можно было бы рассмотреть возможность продления жизни токена в результате каких-либо действий пользователя.

UPD: Приложение хранит локи в памяти - соотственно обладает состоянием и не может горизонтально масштабироваться :(