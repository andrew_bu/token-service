package ru.ortex.domain;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@SequenceGenerator(name = "token_seq", sequenceName = "token_seq")
public class Token {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "token_seq")
  public Long id;

  @Column(nullable = false)
  public String value;

  @Column(nullable = false)
  public boolean valid;

  @Column(nullable = false, name = "expired_date")
  public LocalDateTime expiredDate;

  @Column(nullable = false, name = "date_created")
  public LocalDateTime dateCreated;

  @ManyToOne(optional = false)
  public User user;

  public Token() {
  }

  public Token(String value, LocalDateTime dateCreated, User user, Duration liveDuration) {
    this.value = value;
    this.dateCreated = dateCreated;
    this.user = user;
    this.valid = true;
    this.expiredDate = dateCreated.plus(liveDuration);
  }
}
