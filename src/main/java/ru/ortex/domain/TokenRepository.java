package ru.ortex.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

  Token findByUserAndValidTrue(User user);

  Token findByValueAndValidTrue(String token);

  long countByUserAndValidTrue(User user);
}
