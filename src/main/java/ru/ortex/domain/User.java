package ru.ortex.domain;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "user_seq", sequenceName = "user_seq")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
  public Long id;

  @Column(unique = true, nullable = false)
  public String login;

  @Column(nullable = false)
  public String password;

  public User() {
  }

  public User(String login, String password) {
    this.login = login;
    this.password = password;
  }
}
