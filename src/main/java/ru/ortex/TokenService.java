package ru.ortex;

import com.google.common.util.concurrent.Striped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.ortex.domain.Token;
import ru.ortex.domain.TokenRepository;
import ru.ortex.domain.User;
import ru.ortex.domain.UserRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.locks.Lock;

@Service
public class TokenService {

  private UserRepository userRepository;
  private TokenRepository tokenRepository;
  private TokenHashMaker tokenHashMaker;

  @Value("${token.duration}")
  private String tokenDuration;

  private Striped<Lock> striped = Striped.lazyWeakLock(5); // кол-во stripes необходимо выставлять с учетом нагрузки

  @Autowired
  public TokenService(UserRepository userRepository, TokenRepository tokenRepository, TokenHashMaker tokenHashMaker) {
    this.userRepository = userRepository;
    this.tokenRepository = tokenRepository;
    this.tokenHashMaker = tokenHashMaker;
  }

  public Result authorize(String login, String password) {

    User user = userRepository.findByLoginAndPassword(login, password);
    if (user == null) {
      return Result.WRONG_USER_CREDENTIALS;
    }

    Lock lock = striped.get(login);
    lock.lock();
    try {
      Token token = tokenRepository.findByUserAndValidTrue(user);
      if (token != null) {
        token.valid = false;
        tokenRepository.save(token);
      }

      LocalDateTime now = LocalDateTime.now();
      String tokenValue = tokenHashMaker.makeHash(user.login, now);
      Token newToken = new Token(tokenValue, now, user, Duration.parse(tokenDuration));
      tokenRepository.save(newToken);

      return Result.accepted(newToken.value);
    } finally {
      lock.unlock();
    }
  }

  public Result verify(String tokenValue) {

    Token token = tokenRepository.findByValueAndValidTrue(tokenValue);
    if (token == null) {
      return Result.NOT_FOUND;
    }

    if (token.expiredDate.isBefore(LocalDateTime.now())) {
      token.valid = false;
      tokenRepository.save(token);
      return Result.NOT_FOUND;
    }

    return Result.ok(token.user.login);
  }

}
