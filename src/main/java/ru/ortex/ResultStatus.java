package ru.ortex;

import org.springframework.http.HttpStatus;

public enum ResultStatus {
  Accepted(HttpStatus.ACCEPTED),
  NotFound(HttpStatus.NOT_FOUND),
  WrongCredentials(HttpStatus.BAD_REQUEST),
  Ok(HttpStatus.OK);

  public final HttpStatus httpStatus;

  ResultStatus(HttpStatus httpStatus) {
    this.httpStatus = httpStatus;
  }
}
