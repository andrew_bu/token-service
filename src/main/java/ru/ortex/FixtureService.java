package ru.ortex;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ortex.domain.User;
import ru.ortex.domain.UserRepository;

import javax.annotation.PostConstruct;

@Service
public class FixtureService {

  private final UserRepository userRepository;

  @Autowired
  public FixtureService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @PostConstruct
  public void init() {
    userRepository.save(new User("John", "12345"));
    userRepository.save(new User("George", "123456"));
  }

}
