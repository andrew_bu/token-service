package ru.ortex;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {

  private TokenService tokenService;

  @Autowired
  public TokenController(TokenService tokenService) {
    this.tokenService = tokenService;
  }

  @ApiOperation("Авторизация")
  @RequestMapping(path = "/authorize", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded")
  public ResponseEntity<String> authorize(@RequestParam String login, @RequestParam String password) {
    Result result = tokenService.authorize(login, password);
    return result.toResponseEntity();
  }

  @ApiOperation("Верификация токена")
  @RequestMapping(path = "/verify", method = RequestMethod.GET)
  public ResponseEntity<String> verify(String token) {
    Result result = tokenService.verify(token);
    return result.toResponseEntity();
  }
}
