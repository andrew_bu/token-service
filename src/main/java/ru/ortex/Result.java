package ru.ortex;

import org.springframework.http.ResponseEntity;

public class Result {

  public final static Result WRONG_USER_CREDENTIALS = new Result(ResultStatus.WrongCredentials, "Wrong user credentials");
  public final static Result NOT_FOUND = new Result(ResultStatus.NotFound, "Not Found");

  public final ResultStatus status;
  public final String response;

  public Result(ResultStatus status, String response) {
    this.status = status;
    this.response = response;
  }

  public ResponseEntity<String> toResponseEntity() {
    return ResponseEntity.status(status.httpStatus).body(response);
  }

  public static Result accepted(String response) {
    return new Result(ResultStatus.Accepted, response);
  }

  public static Result ok(String response) {
    return new Result(ResultStatus.Ok, response);
  }

}
