Feature: authorize
  Scenario: Issue a token with correct credentials
    Given there is a user John with password 12345
    When I issue a token as John with password 12345
    Then I see response with valid token with code 202

  Scenario: Issue a token with wrong credentials
    Given there is a user John with password 12345
    When I issue a token as John with password 12346
    Then I see response "Wrong user credentials" with code 400