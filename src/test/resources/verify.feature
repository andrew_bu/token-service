Feature: verify
  Scenario: Get user login by token
    Given there is a user John with token 6b9b9319
    When I ask for user login with token 6b9b9319
    Then I see response "John" with code 200

  Scenario: Get user login by unexisting token
    Given there is a user John with token 6b9b9319
    When I ask for user login with token 7b9b9319
    Then I see response "Not Found" with code 404
