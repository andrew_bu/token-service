package ru.ortex;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import ru.ortex.domain.Token;
import ru.ortex.domain.TokenRepository;
import ru.ortex.domain.User;
import ru.ortex.domain.UserRepository;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@ContextConfiguration(classes = TokenServiceApplication.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class StepDefs {

  private TestRestTemplate restTemplate = new TestRestTemplate();
  private ResponseEntity<String> latestResponse;

  @Autowired
  private TokenRepository tokenRepository;

  @Autowired
  private UserRepository userRepository;

  @Before
  public void setUp() {
    tokenRepository.deleteAll();
    userRepository.deleteAll();
  }

  @Given("^there is a user (\\w+) with password (\\w+)$")
  public void there_is_a_user_with_password(String login, String password) {
    userRepository.save(new User(login, password));
  }

  @Given("^there is a user (\\w+) with token (\\w+)$")
  public void there_is_a_user_with_token(String login, String token) {
    User user = userRepository.save(new User(login, "password"));
    tokenRepository.save(new Token(token, LocalDateTime.now(), user, Duration.ofHours(1)));
  }

  @When("^I issue a token as (\\w+) with password (\\w+)$")
  public void i_issue_a_token_as_login_with_password(String login, String password) {
    latestResponse = authorize(login, password);
  }

  @When("^I ask for user login with token (\\w+)")
  public void i_ask_for_user_login_with_token(String token) {
    latestResponse = verify(token);
  }

  @Then("^I see response with valid token with code (\\d+)$")
  public void i_see_response_with_valid_token_with_code(int code) {
    assertNotNull(latestResponse.getBody());
    assertEquals(code, latestResponse.getStatusCodeValue());
  }

  @Then("^I see response \"(.*)\" with code (\\d+)$")
  public void i_see_response_valid_token_with_code(String body, int code) {
    assertEquals(body, latestResponse.getBody());
    assertEquals(code, latestResponse.getStatusCodeValue());
  }

  private ResponseEntity<String> authorize(String login, String password) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("login", login);
    params.add("password", password);

    HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(params, headers);
    return restTemplate.exchange("http://localhost:8080/authorize", HttpMethod.POST, httpEntity, String.class);
  }

  private ResponseEntity<String> verify(String token) {
    UriComponents uriComponents = UriComponentsBuilder
      .fromHttpUrl("http://localhost:8080/verify")
      .queryParam("token", token)
      .build();

    return restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, HttpEntity.EMPTY, String.class);
  }
}
