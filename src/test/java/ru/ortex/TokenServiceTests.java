package ru.ortex;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.ortex.domain.TokenRepository;
import ru.ortex.domain.User;
import ru.ortex.domain.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = "token.duration: PT1S")
public class TokenServiceTests {

  @Autowired
  private TokenService tokenService;

  @Autowired
  private TokenRepository tokenRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private FixtureService fixtureService;

  @Before
  public void setUp() throws Exception {
    tokenRepository.deleteAll();
    userRepository.deleteAll();
    fixtureService.init();
  }

  @Test
  public void successAuthorize() {
    Result authorizeResult = tokenService.authorize("John", "12345");
    assertEquals(authorizeResult.status, ResultStatus.Accepted);

    String token = authorizeResult.response;
    assertNotNull(token);

    Result verifyResult = tokenService.verify(token);
    assertEquals(verifyResult.status, ResultStatus.Ok);
    assertEquals(verifyResult.response, "John");
  }

  @Test
  public void unSuccessAuthorize() {
    Result authorizeResult = tokenService.authorize("login", "password");
    assertEquals(authorizeResult.status, ResultStatus.WrongCredentials);
    assertEquals(authorizeResult.response, "Wrong user credentials");
  }

  @Test
  public void verifyFail() {
    Result verifyResult = tokenService.verify("111");
    assertEquals(verifyResult.status, ResultStatus.NotFound);
    assertEquals(verifyResult.response, "Not Found");
  }

  @Test
  public void differentTokens() {
    String token1 = tokenService.authorize("John", "12345").response;
    String token2 = tokenService.authorize("John", "12345").response;
    assertNotEquals(token1, token2);
  }

  @Test
  public void tokenExpired() throws InterruptedException {
    String token = tokenService.authorize("John", "12345").response;

    Result verifyResult1 = tokenService.verify(token);
    assertEquals(verifyResult1.status, ResultStatus.Ok);

    Thread.sleep(1000);

    Result verifyResult2 = tokenService.verify(token);
    assertEquals(verifyResult2.status, ResultStatus.NotFound);
  }

  @Test
  public void raceConditionAuthorize() throws InterruptedException {
    List<Callable<Result>> tasks = new ArrayList<>();
    for (int i = 0; i < 20; i++) {
      tasks.add(() -> tokenService.authorize("John", "12345"));
    }

    ExecutorService executorService = Executors.newFixedThreadPool(4);
    executorService.invokeAll(tasks);

    User john = userRepository.findByLoginAndPassword("John", "12345");
    long countTokens = tokenRepository.countByUserAndValidTrue(john);
    assertEquals(countTokens, 1L);
  }
}
